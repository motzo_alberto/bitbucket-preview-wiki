import sublime, sublime_plugin
import urllib2, urllib, io, json
import os.path, webbrowser

class ExampleCommand(sublime_plugin.TextCommand):
  def run(self, edit, **args):

    debug = 0
    wikiFilePath = self.view.file_name()
    markup_type = args['markup_type']
    if debug :
      pass 
      print('Processing: '+wikiFilePath)
      print('Markup: '+markup_type)

    # Reading file content
    wikiFile = open(wikiFilePath, 'r')
    wikiFileContent = wikiFile.read()
    wikiFile.close()

    if debug :
      pass 
      print('Wiki Content +++++++++++++++++++++++++')
      print(wikiFileContent)

    url = 'https://bitbucket.org/api/1.0/content-preview/'
    values = {'content':wikiFileContent,'markup_type':markup_type}

    data = urllib.urlencode(values)
    req = urllib2.Request(url, data)
    response = urllib2.urlopen(req)
    pageContent = response.read()

    # saving preview file
    result = json.loads(pageContent)
    outputFile =os.path.join(os.path.dirname(wikiFilePath),'preview_wiki_ouput_ignore.html')
    
    previewFile = open(outputFile, 'w')
    
    if debug :
      pass 
      print('HTML Content +++++++++++++++++++++++++')
      print(result)
    previewFile.write(result['markup'])
    previewFile.close()

    webbrowser.open(outputFile)
